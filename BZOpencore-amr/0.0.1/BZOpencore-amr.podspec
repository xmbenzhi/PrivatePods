
Pod::Spec.new do |s|

  s.name         = "BZOpencore-amr"
  s.version      = "0.0.1"
  s.summary      = "A short description of BZOpencore-amr."

  s.description  = <<-DESC
                    厦门本智科技ios音频转换工具
                   DESC

  s.homepage     = "http://opencore-amr.sourceforge.net"
  s.license      = "MIT"
  s.author       = { "郭艺伟" => "912723609@qq.com" }
  #s.platform     = :ios, "8.0"
  s.ios.deployment_target = '8.0'
  s.osx.deployment_target = '10.9'

  s.source       = { :git => "https://gitee.com/xmbenzhi/BZOpencore-amr.git", :tag => "#{s.version}" }
  s.source_files  = 'headers/*.{h,m}'
  s.public_header_files = "headers/*.h"
  #s.public_header_files = 'headers/*.h'
  s.preserve_paths = "headers/**", "lib/**"
  #s.preserve_paths = 'headers/**', 'lib/**'
  s.vendored_library  = 'lib/libopencore-amrnb.a'
  #s.dependency 'AFNetworking', '~> 3.1.0'
end
