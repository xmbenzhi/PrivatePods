
Pod::Spec.new do |s|

  s.name         = "BZDataBaseSDK"
  s.version      = "0.0.5"
  s.summary      = "A short description of BZDataBaseSDK."

  s.description  = <<-DESC
                    掌中佛平台用户中心组件
                   DESC

  s.homepage     = "http://EXAMPLE/BZDataBaseSDK"
  s.license      = "MIT"
  s.author       = { "瞿衡" => "quhengwork@163.com" }
  s.author       = { "quheng" => "quhengwork@163.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/xmbenzhi/BZDataBaseSDK.git", :tag => "#{s.version}" }
  s.public_header_files = 'BZDataBaseSDK/*.h', 'BZDataBaseSDK/include/**/*.h'
  s.source_files = 'BZDataBaseSDK/**/*.{h,m,pch}'
  s.requires_arc = true

    s.dependency 'FMDB'
    s.dependency 'BZPreHeaderComponent'
end
