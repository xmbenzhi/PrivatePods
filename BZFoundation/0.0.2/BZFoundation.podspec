
Pod::Spec.new do |s|

  s.name         = "BZFoundation"
  s.version      = "0.0.2"
  s.summary      = "A short description of BZFoundation."

  s.description  = <<-DESC
                    厦门本智科技ios基础框架
                   DESC

  s.homepage     = "http://EXAMPLE/BZFoundation"
  s.license      = "MIT"
  s.author       = { "郭艺伟" => "912723609@qq.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/xmbenzhi/BZFoundation.git", :tag => "#{s.version}" }
  s.public_header_files = 'BZFoundation/*.h', 'BZFoundation/include/**/*.h'
  s.source_files = 'BZFoundation/**/*.{h,m,c}'
  #s.requires_arc = true
  #引用静态库：“(.ios).library”。去掉头尾的lib，用“,”分割 。注意： (.ios)括号括起来代表可以省略
  #引用libxml2.lib和libz.lib.   
  #s.libraries = 'xml2', 'z'
  s.library = 'z'

  s.dependency 'BZOpencore-amr'
end
