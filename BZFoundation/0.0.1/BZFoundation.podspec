
Pod::Spec.new do |s|

  s.name         = "BZFoundation"
  s.version      = "0.0.1"
  s.summary      = "A short description of BZFoundation."

  s.description  = <<-DESC
                    厦门本智科技ios基础视图
                   DESC

  s.homepage     = "http://EXAMPLE/BZFoundation"
  s.license      = "MIT"
  s.author       = { "郭艺伟" => "912723609@qq.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/xmbenzhi/BZFoundation.git", :tag => "#{s.version}" }
  s.public_header_files = 'BZFoundation/*.h', 'BZFoundation/include/**/*.h'
  s.source_files = 'BZFoundation/**/*.{h,m}'
  s.requires_arc = true

  s.dependency 'AFNetworking', '~> 3.1.0'
end
