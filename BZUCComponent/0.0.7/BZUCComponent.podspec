
Pod::Spec.new do |s|

  s.name         = "BZUCComponent"
  s.version      = "0.0.7"
  s.summary      = "A short description of BZUCComponent."

  s.description  = <<-DESC
                    掌中佛平台登录注册组件
                   DESC

  s.homepage     = "http://EXAMPLE/BZUCComponent"
  s.license      = "MIT"
  s.author       = { "瞿衡" => "quhengwork@163.com" }
  s.author       = { "quheng" => "quhengwork@163.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/xmbenzhi/BZUCComponent.git", :tag => "#{s.version}" }
  s.public_header_files = 'BZUCComponent/*.h', 'BZUCComponent/include/**/*.h'
  s.source_files = 'BZUCComponent/**/*.{h,m,pch}'
  s.requires_arc = true

    s.dependency 'FMDB'
    s.dependency 'BZPreHeaderComponent'
    s.dependency 'BZDataBaseSDK'
    s.dependency 'Masonry'
    s.dependency 'BZBaseNetWorkSDK'
    s.dependency 'BZBaseWidget'
    s.dependency 'YYKit'
    s.dependency 'libextobjc'
    s.dependency 'BZBundleKit'
    s.dependency 'BZUCSDK'
    s.dependency 'UMengUShare/UI'
    s.dependency 'UMengUShare/Social/ReducedWeChat'
    s.dependency 'UMengUShare/Social/ReducedQQ'
    s.dependency 'UMengUShare/Social/ReducedSina'
end
