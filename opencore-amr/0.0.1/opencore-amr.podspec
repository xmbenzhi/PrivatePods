
Pod::Spec.new do |s|

  s.name         = "opencore-amr"
  s.version      = "0.0.1"
  s.summary      = "A short description of opencore-amr."

  s.description  = <<-DESC
                    厦门本智科技ios基础视图
                   DESC

  s.homepage     = "http://EXAMPLE/opencore-amr"
  s.license      = "MIT"
  s.author       = { "郭艺伟" => "912723609@qq.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/xmbenzhi/opencore-amr.git", :tag => "#{s.version}" }
  s.public_header_files = 'opencore-amr/*.h', 'opencore-amr/include/**/*.h'
  s.source_files = 'opencore-amr/**/*.{h,m}'
  s.requires_arc = true
end
