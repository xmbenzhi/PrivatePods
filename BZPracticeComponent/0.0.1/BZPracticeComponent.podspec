
Pod::Spec.new do |s|

  s.name         = "BZPracticeComponent"
  s.version      = "0.0.1"
  s.summary      = "A short description of BZPracticeComponent."

  s.description  = <<-DESC
                    厦门本智科技ios基础视图
                   DESC

  s.homepage     = "http://EXAMPLE/BZPracticeComponent"
  s.license      = "MIT"
  s.author       = { "郭艺伟" => "912723609@qq.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/xmbenzhi/BZPracticeComponent.git", :tag => "#{s.version}" }
  s.public_header_files = 'BZPracticeComponent/*.h', 'BZPracticeComponent/include/**/*.h'
  s.source_files = 'BZPracticeComponent/**/*.{h,m}'
  s.requires_arc = true

  s.dependency 'AFNetworking', '~> 3.1.0'
  s.dependency 'ReactiveCocoa','~>2.5.0'
  s.dependency 'Masonry'
  s.dependency 'UMengAnalytics', '~> 4.2.4'
  s.dependency 'DZNEmptyDataSet', '~> 1.8.1'
  s.dependency 'MJRefresh'
end
