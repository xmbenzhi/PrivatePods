
Pod::Spec.new do |s|

  s.name         = "BZPreHeaderComponent"
  s.version      = "0.0.2"
  s.summary      = "A short description of BZPreHeaderComponent."

  s.description  = <<-DESC
                    掌中佛平台用户中心组件
                   DESC

  s.homepage     = "http://EXAMPLE/BZPreHeaderComponent"
  s.license      = "MIT"
  s.author       = { "瞿衡" => "quhengwork@163.com" }
  s.author       = { "quheng" => "quhengwork@163.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/xmbenzhi/BZPreHeaderComponent.git", :tag => "#{s.version}" }
  s.public_header_files = 'BZPreHeaderComponent/*.h', 'BZPreHeaderComponent/include/**/*.h'
  s.source_files = 'BZPreHeaderComponent/**/*.{h,m}'
  s.requires_arc = true

    s.dependency 'AFNetworking', '~> 3.1.0'
end
