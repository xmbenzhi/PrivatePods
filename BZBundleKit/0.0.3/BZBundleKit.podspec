
Pod::Spec.new do |s|

  s.name         = "BZBundleKit"
  s.version      = "0.0.3"
  s.summary      = "A short description of BZBundleKit."

  s.description  = <<-DESC
                    掌中佛平台用户中心组件
                   DESC

  s.homepage     = "http://EXAMPLE/BZBundleKit"
  s.license      = "MIT"
  s.author       = { "瞿衡" => "quhengwork@163.com" }
  s.author       = { "quheng" => "quhengwork@163.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/xmbenzhi/BZBundleKit.git", :tag => "#{s.version}" }
  s.public_header_files = 'BZBundleKit/*.h', 'BZBundleKit/include/**/*.h'
  s.source_files = 'BZBundleKit/**/*.{h,m,pch}'
  s.requires_arc = true

    s.dependency 'BZPreHeaderComponent'

end
