
Pod::Spec.new do |s|

  s.name         = "BZBaseViewComponent"
  s.version      = "0.0.2"
  s.summary      = "A short description of BZBaseViewComponent."

  s.description  = <<-DESC
                    厦门本智科技ios基础视图
                   DESC

  s.homepage     = "http://EXAMPLE/BZBaseViewComponent"
  s.license      = "MIT"
  s.author       = { "郭艺伟" => "912723609@qq.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/xmbenzhi/BZBaseViewComponent.git", :tag => "#{s.version}" }
  s.public_header_files = 'BZBaseViewComponent/*.h', 'BZBaseViewComponent/include/**/*.h'
  s.source_files = 'BZBaseViewComponent/**/*.{h,m}'
  s.requires_arc = true

  s.dependency 'AFNetworking', '~> 3.1.0'
  s.dependency 'ReactiveCocoa','~>2.5.0'
  s.dependency 'Masonry'

end
