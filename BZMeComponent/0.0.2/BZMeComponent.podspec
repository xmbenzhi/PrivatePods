
Pod::Spec.new do |s|

  s.name         = "BZMeComponent"
  s.version      = "0.0.2"
  s.summary      = "A short description of BZMeComponent."

  s.description  = <<-DESC
                    厦门本智科技ios基础视图
                   DESC

  s.homepage     = "http://EXAMPLE/BZMeComponent"
  s.license      = "MIT"
  s.author       = { "郭艺伟" => "912723609@qq.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/xmbenzhi/BZMeComponent.git", :tag => "#{s.version}" }
  s.public_header_files = 'BZMeComponent/*.h', 'BZMeComponent/include/**/*.h'
  s.source_files = 'BZMeComponent/**/*.{h,m}'
  s.requires_arc = true

  s.dependency 'libextobjc'
  s.dependency 'SDWebImage'
  s.dependency 'Mantle'

  s.dependency 'BZFoundation'
  s.dependency 'BZBaseViewComponent'
  s.dependency 'BZPreHeaderComponent'
  s.dependency 'BZDataBaseSDK'
  s.dependency 'BZBaseNetWorkSDK'
  s.dependency 'BZBundleKit'
  s.dependency 'BZBaseWidget'
end
