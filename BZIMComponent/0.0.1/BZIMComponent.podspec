
Pod::Spec.new do |s|

  s.name         = "BZIMComponent"
  s.version      = "0.0.1"
  s.summary      = "A short description of BZIMComponent."

  s.description  = <<-DESC
                    厦门本智科技ios基础视图
                   DESC

  s.homepage     = "http://EXAMPLE/BZIMComponent"
  s.license      = "MIT"
  s.author       = { "郭艺伟" => "912723609@qq.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/xmbenzhi/BZIMComponent.git", :tag => "#{s.version}" }
  s.public_header_files = 'BZIMComponent/*.h', 'BZIMComponent/include/**/*.h'
  s.source_files = 'BZIMComponent/**/*.{h,m}'
  s.requires_arc = true

  s.dependency 'AFNetworking', '~> 3.1.0'
  s.dependency 'Masonry'
  s.dependency 'MJRefresh'
end
