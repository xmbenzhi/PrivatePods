
Pod::Spec.new do |s|

  s.name         = "BZBaseWidget"
  s.version      = "0.0.2"
  s.summary      = "A short description of BZBaseWidget."

  s.description  = <<-DESC
                    掌中佛平台用户中心组件
                   DESC

  s.homepage     = "http://EXAMPLE/BZBaseWidget"
  s.license      = "MIT"
  s.author       = { "瞿衡" => "quhengwork@163.com" }
  s.author       = { "quheng" => "quhengwork@163.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/xmbenzhi/BZBaseWidget.git", :tag => "#{s.version}" }
  s.public_header_files = 'BZBaseWidget/*.h', 'BZBaseWidget/include/**/*.h'
  s.source_files = 'BZBaseWidget/**/*.{h,m,pch}'
  s.requires_arc = true

    s.dependency 'FMDB'
    s.dependency 'BZPreHeaderComponent'
    s.dependency 'YYKit'
    s.dependency 'Masonry'

end
