
Pod::Spec.new do |s|

  s.name         = "BZBaseNetWorkSDK"
  s.version      = "0.0.9"
  s.summary      = "A short description of BZBaseNetWorkSDK."

  s.description  = <<-DESC
                    掌中佛平台用户中心组件
                   DESC

  s.homepage     = "http://EXAMPLE/BZBaseNetWorkSDK"
  s.license      = "MIT"
  s.author       = { "瞿衡" => "quhengwork@163.com" }
  s.author       = { "quheng" => "quhengwork@163.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/xmbenzhi/BZBaseNetworkSDK.git", :tag => "#{s.version}" }
  s.public_header_files = 'BZBaseNetWorkSDK/*.h', 'BZBaseNetWorkSDK/include/**/*.h'
  s.source_files = 'BZBaseNetWorkSDK/**/*.{h,m}'
  s.requires_arc = true

    s.dependency 'AFNetworking', '~> 3.1.0'
    s.dependency 'MBProgressHUD'
    s.dependency 'AFNetworking'
    s.dependency 'BZPreHeaderComponent'
    s.dependency 'SVProgressHUD'
    s.dependency 'Bugly'
    s.dependency 'Qiniu'
    s.dependency 'Mantle'

end
