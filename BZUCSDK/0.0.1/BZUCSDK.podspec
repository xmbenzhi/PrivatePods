
Pod::Spec.new do |s|

  s.name         = "BZUCSDK"
  s.version      = "0.0.1"
  s.summary      = "A short description of BZUCSDK."

  s.description  = <<-DESC
                    厦门本智科技ios基础视图
                   DESC

  s.homepage     = "http://EXAMPLE/BZUCSDK"
  s.license      = "MIT"
  s.author       = { "郭艺伟" => "912723609@qq.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/xmbenzhi/BZUCSDK.git", :tag => "#{s.version}" }
  s.public_header_files = 'BZUCSDK/*.h', 'BZUCSDK/include/**/*.h'
  s.source_files = 'BZUCSDK/**/*.{h,m}'
  s.requires_arc = true

  s.dependency 'AFNetworking', '~> 3.1.0'
  s.dependency 'Masonry'
  s.dependency 'Mantle'
  s.dependency 'MJRefresh'
end
