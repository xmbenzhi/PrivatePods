
Pod::Spec.new do |s|

  s.name         = "BZResourcesBundle"
  s.version      = "0.0.3"
  s.summary      = "A short description of BZResourcesBundle."

  s.description  = <<-DESC
                    掌中佛平台祈愿组件
                   DESC

  s.homepage     = "http://EXAMPLE/BZResourcesBundle"
  s.license      = "MIT"
  s.author       = { "quheng" => "quhengwork@163.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/xmbenzhi/BZResourcesBundle.git", :tag => "#{s.version}" }

    s.resource = 'BZResourcesBundle.bundle'

  #s.public_header_files = 'BZResourcesBundle/*.h', 'BZResourcesBundle/include/**/*.h'
  #s.source_files = 'BZResourcesBundle.bundle/**'
  #s.requires_arc = true

end
